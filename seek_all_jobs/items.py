import scrapy


class JobItem(scrapy.Item):
    date_posted = scrapy.Field()
    location = scrapy.Field()
    title = scrapy.Field()
    company_name = scrapy.Field()
    star_rating = scrapy.Field()
    description = scrapy.Field()
    link_to_apply = scrapy.Field()
    source_url = scrapy.Field()
    date_posted = scrapy.Field()
    
    def __repr__(self):
        return repr({
            "title": self["title"],
            "source_url": self["source_url"]
        })