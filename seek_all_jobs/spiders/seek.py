from scrapy.exceptions import CloseSpider
from urllib.parse import urlencode
from datetime import datetime
from ..items import JobItem
import scrapy
import json

class SeekSpider(scrapy.Spider):
    name = 'seek'
    allow_domains = ['seek.com.au']
    base_url = "https://www.seek.com.au"

    headers = {
        "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "cookie": "JobseekerSessionId=c90a9658501a513e5e17e4ffab54ac80; JobseekerVisitorId=c90a9658501a513e5e17e4ffab54ac80; responsive-trial=chrome:42; _hjid=d425288b-3093-4f68-b61d-0f421cbdd9f1; mp_bec1ab45277e4973c862f8b3c43fd6fa_mixpanel=%7B%22distinct_id%22%3A%20%221748be3562cb29-0971334d4f2368-1d251809-2a3000-1748be3562db06%22%2C%22%24device_id%22%3A%20%221748be3562cb29-0971334d4f2368-1d251809-2a3000-1748be3562db06%22%2C%22%24initial_referrer%22%3A%20%22https%3A%2F%2Fwww.seek.com.au%2Fprivacy-consent%2F%3FreturnUrl%3D%2F%22%2C%22%24initial_referring_domain%22%3A%20%22www.seek.com.au%22%7D; _ga=GA1.3.495679050.1600074766; _gcl_au=1.1.2139826666.1600074766; s_ecid=MCMID%7C16063890287220550561599172400959808172; _scid=79c0ee54-642c-4cc5-8525-870150d5fdb4; sol_id=734a1ca1-3f9e-479a-9671-6b4253d605b5; sol_id=734a1ca1-3f9e-479a-9671-6b4253d605b5; sol_id_pre_stored=734a1ca1-3f9e-479a-9671-6b4253d605b5; requires_consent_for_pii=true; ASP.NET_SessionId=5gqdhuuniswxpukidmmr2gpb; performed_consent_check=loggedOut; sol_id_initialized=734a1ca1-3f9e-479a-9671-6b4253d605b5; _hjTLDTest=1; _hjAbsoluteSessionInProgress=1; _gid=GA1.3.1566622510.1601462106; AMCVS_199E4673527852240A490D45%40AdobeOrg=1; AMCV_199E4673527852240A490D45%40AdobeOrg=-1712354808%7CMCIDTS%7C18536%7CMCMID%7C16063890287220550561599172400959808172%7CMCAAMLH-1602066906%7C6%7CMCAAMB-1602066906%7CRKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y%7CMCOPTOUT-1601469306s%7CNONE%7CMCAID%7CNONE%7CvVersion%7C4.3.0; s_cc=true; _pin_unauth=dWlkPVpEZzRZVGhrWWpRdFltUTRaQzAwTkRjMUxXRTVNalF0WVRWaE1XSm1PV1V5Tm1ZMiZycD1kSEoxWlE; s_sq=%5B%5BB%5D%5D; main=V%7C2~P%7Cjobsearch~K%7Csoftware%20engineer~WID%7C3000~L%7C3000~OSF%7Cquick&set=1601463713552; searchTerm=software engineer; skl-lcid=67b323b7-fff1-4251-9eb2-33bdb7f3d4c0; utag_main=v_id:01748be3421a00211d68c5ed710803068003406000e50$_sn:2$_se:32$_ss:0$_st:1601465516362$vapi_domain:seek.com.au$ses_id:1601462098036%3Bexp-session$_pn:21%3Bexp-session$krux_sync_session:1601462098036%3Bexp-session",
        "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36"
    }

    def __init__(self, *args, **kwargs):
        self.keywords = None
        self.daterange = int(kwargs.get("daterange")) 

        if not self.daterange:
            raise CloseSpider("Data range value must be set")

    def start_requests(self):
        yield scrapy.Request(
            'https://www.seek.com.au/', self.parse, headers = self.headers)

    def parse(self, response):
        if not self.keywords:
            search_url = f"https://www.seek.com.au/jobs?daterange={self.daterange}&sortmode=ListedDate"

            yield scrapy.Request(
                search_url, self.parse_list_results, headers = self.headers)
        else:
            print("Error in parser")

    def parse_list_results(self, response):
        jobs = response.xpath('//article').getall()

        for job in jobs: 
            sel = scrapy.Selector(text=job)
            job_url = sel.xpath("//a[@data-automation='jobTitle']/@href").get()

            yield response.follow(job_url, self.parse_job)

        next_page = response.xpath(
            '//a[@data-automation="page-next"]/@href').get()
        if next_page:
            yield response.follow(next_page, self.parse_list_results, meta=response.meta)

    def parse_job(self, response):
        """
        path = '/home/azureuser/projects/seek-all-jobs/seek_all_jobs/results.txt'
        with open(path, 'a') as f:
            f.write(str(response.text))
            f.write('\n')
        """
        title = response.xpath(
            '//span[@data-automation="job-detail-title"]/span/h2/text()').get()
        company_name = response.xpath(
            '//span[@data-automation="job-header-company-review-title"]/span/text()'
        ).get()
        if not company_name:
            company_name = response.xpath(
                '//span[@data-automation="advertiser-name"]/span/text()'
            ).get()
        link_to_apply = response.xpath(
            '//a[@data-automation="job-detail-apply"]/@href').get()
        description = response.xpath(
            '//div[@data-automation="mobileTemplate"]//text()').getall()
        description = "".join(description)
        star_rating = response.xpath(
            '//span/span[contains(text(), "overall rating")]/../text()').get()
        main_address = response.xpath(
            '//section[@aria-labelledby="jobInfoHeader"]/dl/dd[not(@data-automation="job-detail-date")]/span/span/strong/text()').get()
        secondary_address = response.xpath(
            '//section[@aria-labelledby="jobInfoHeader"]/dl/dd[not(@data-automation="job-detail-date")]/span/span/span/text()').get()
        location = main_address+","+secondary_address if secondary_address != None else main_address##add location address

        date_text = response.xpath(
            '//dd[@data-automation="job-detail-date"]/span/span/text()').get()

        from datetime import datetime, timedelta

        try:
            today = datetime.now()
            job_date = datetime.strptime(date_text, '%d %b %Y')

            time_diff_day = today-job_date
            days = time_diff_day.days

            if days == 0:
                date_text = 'Today'
            elif days == 1:
                date_text = f'{days} day ago'
            elif days < 30:
                date_text = f'{days} days ago'
            elif days > 30:
                date_text = f'{30}+ days ago'
        except:
            date_text = None 


        yield JobItem(
            date_posted = date_text,
            title = title,
            location = location,
            company_name = company_name,
            star_rating = star_rating,
            description = description,
            link_to_apply = f"{self.base_url}{link_to_apply}",
            source_url = response.url,
        )