# Seek job scraper

## Description
Query ALL the jobs from Seek Australia page, saving this data as json or csv.

## Preparing local enviroment
Create a python enviroment:  
`python3 -m venv env`  

Upgrade pip package:  
`pip install --upgrade pip`  

Procede to install requirements:  
`pip install -r requirements.txt`  

## Requesting and saving the jobs
There is only one parameter **"-a daterange"**  
This value is required and represents the number of days since the job has been posted and can be between 1-999 where 999 means 'any' date in the platform.  
**"-o"** specifies the name and extension of the output file.
  
To run to job search use:
```
scrapy crawl seek -a daterange=1 -o output.json
```
**".json"** gives better results for saving but it is also possible to save them as csv but can give many "null" or "empty" column results and this is a scrappy core issue.  
```
scrapy crawl seek -a daterange=1 -o output.csv
```
**NOTE**: Testing with "daterange=1" can give few results, but using 2 or 3 days can give substantially more results including jobs posted "Today" that are correct, this is an internal SEEK issue. 